require_relative "../spec_helper"

describe "gitlab-vault::default" do
  context "With ubuntu 16.04" do
    let(:chef_run) {
      ChefSpec::SoloRunner.new(version: "16.04").converge(described_recipe)
    }
  end

  context "With ubuntu 14.04" do
    let(:chef_run) {
      ChefSpec::SoloRunner.new(version: "14.04").converge(described_recipe)
    }
  end
end
